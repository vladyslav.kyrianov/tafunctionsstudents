using System;
using System.Linq;

namespace TAFunctions
{
    public enum SortOrder { Ascending, Descending };
    static public class OperationsWithArrays
    {
        /// <summary>
        /// Should sort array in SortOrder order
        /// <exception cref="ArgumentNullException">Thrown when wrong parameter array</exception>
        /// </summary>

        public static void SortArray(int[] array, SortOrder order)
        {
            if (order == SortOrder.Ascending)
            {
                for (int i = 0; i < array.Length; i++)
                {
                    array.OrderBy(el => array[i]);
                }
            }
            else if(order == SortOrder.Descending)
            {
                for (int i = 0; i < array.Length; i++)
                {
                    array.OrderByDescending(el => array[i]);
                }
            }
            else 
            {
                throw new ArgumentNullException("wrong parameter array");
            }
        }

        /// <summary>
        /// Should to check whether the array is sorted in SortOrder order. An array should be not changed
        /// <exception cref="ArgumentNullException">Thrown when wrong parameter array</exception>
        /// </summary>
        public static bool IsSorted(int[] array, SortOrder order)
        {
            bool isSorted = false;
            if (order == SortOrder.Ascending)
            {
                for (int i = 0; i < array.Length - 1; i++)
                {
                    if (array[i + 1] > array.Length)
                    {
                        isSorted = true;
                    }
                    else isSorted = false;
                    if (isSorted == false)
                    {
                        return false;
                    }
                }
                return isSorted;
            }

            else if (order == SortOrder.Descending)
            {
                for (int i = 0; i < array.Length - 1; i++)
                {
                    if (array[i + 1] < array.Length)
                    {
                        isSorted = true;
                    }
                    else isSorted = false;
                    if (isSorted == false)
                    {
                        return false;
                    }
                }
                return isSorted;
            }
            else
            {
                throw new ArgumentNullException("wrong parameter array");
            }
        }
           
        }        
    }

